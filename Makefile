# ==================================================================== #
#
# Copyright 2019 Philippe Grégoire <pg@pgregoire.xyz>
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#
# ==================================================================== #
.POSIX:

PROJNAME=	punycodes

UPLOAD=\
	upload() { \
		. ./.venv/bin/activate && \
		set -x && \
		pip install -U twine && \
		python -mtwine upload --verbose "$${@}" dist/*; \
	}; upload


all: build


# building
build: distclean venv
	. ./.venv/bin/activate && python setup.py sdist bdist_wheel

venv: .venv
	. ./.venv/bin/activate && \
		pip install -U pip && \
		pip install -U setuptools && \
		pip install -U wheel

.venv:
	python3 -mvenv .venv


# main upload target
upload:
	unset __DEVEL; ${MAKE} build
	${UPLOAD}


# testing
check: upload-test
	rm -rf .testenv && \
	python3 -mvenv .testenv && \
	. ./.testenv/bin/activate && \
	pip install --index-url https://test.pypi.org/simple/ ${PROJNAME} && \
	punycodes domain.tld && \
	punycodes -d xn--mn-jma913a7buh.tld

upload-test:
	export __DEVEL=1; ${MAKE} build
	${UPLOAD} --repository-url https://test.pypi.org/legacy/


# cleaning
clean:
	@:

distclean: clean
	rm -fr build/ dist/ *.egg-info */*.pyc

# ==================================================================== #
