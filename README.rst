=========
punycodes
=========

Encode a domain with look-alike characters and decode encoded
names.

usage
=====

Find look-alike domains (all solutions may not correctly)::

    punycodes domain.tld

Show what an encoded domain looks like::

    punycodes -d xn--mn-kja2l93leya.tld
